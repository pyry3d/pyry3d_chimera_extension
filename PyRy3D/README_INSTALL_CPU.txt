PyRy3D: a software tool for macromolecular structure modeling


(c) 2010 by Joanna M. Kasprzak, Mateusz Dobrych�op, Wojciech Potrzebowski and Janusz M. Bujnicki



Version 4.6.0

Homepage:
http://genesilico.pl/pyry3d/   

PyRy3D server:
http://pyry3d.icm.edu.pl/

Technical Support:
    jkasp@amu.edu.pl

------------------------------------------------------------------------

INSTALLATION INSTRUCTIONS

1. Requirements:

PyRy3D runs on any modern Windows or Linux PC. There is a source code with full functionality, but requires some libraries.

In case you want to use PyRy3D with CRYSOL please first obtain the code and the licence to use it from authors.


2. Installation on Linux

To install PyRy3D on Linux, you need to:

- Download the source distribution

- Unzip the archive. A catalog with the main program pyry3d.py is created.

- Make sure Python 2.5 or a higher version is installed together with and python-dev. (on Ubuntu Linux, use sudo apt-get install python, sudo apt-get install python-dev).

- Make sure Numpy is installed (sudo apt-get install python-numpy).

- Make sure BioPython is installed (sudo apt-get install python-biopython).
   (tested with BioPython 1.54)

- Make sure the PyRy3D/pyry3d.py file is executable:
  chmod a+x PyRy3D/pyry3d.py

- Add the path to the PyRy3D directory to your PYTHONPATH variable,e.g. 

- run:
  python pyry3d.py -h


3. Installing PyRy3D on Windows

To install PyRy3D on Windows, you need to:

- Download the source distribution

- Unzip the archive. A catalog with the main program pyry3d.py is created.

- Make sure that these libraries are installed:
    Python 2.5 or a higher (but NOT 3.0)
    Numpy
    BioPython (tested with BioPython 1.57)

- After this, you can write in the Python shell:

python pyry3d.py -h


------------------------------------------------------------------------------------
how to compile and run

	You need to have these installed on your computer to run optimized version:

		-swig (version 2.0 or newer)
		-python (version 2.5, 2.6 or 2.7) 
		-cmake (version 2.8 or newer)

	Sometimes you will also have to install "python-dev" packages.

	Compilation is simple (here explained linux style):
		1. Enter Modules/Simul/cpp subdirectory in pyry3d directory
		2. `make`
		3. run pyry3d.py 
        If compilation fails try to change path to swig in Modules/Simul/cpp/Makefile from: SWIG = ../../../swig/bin/swig into: SWIG = swig and run 'make' again

	On all systems you have to use cmake to compile pyry's shared library.

	Note that _pyry3d_cpp.so library is not compatible with python3.

	If there are problems with cmake, make sure that it uses correct c++ compiler.
	If there are more than one compiler you may have to specify which one
	should be used by cmake (on Linux you have to set CXX variable to, for
	example, g++-4.7).

	Note that if you're running Pyry under Windows, you need to INSTALL (not only
	download) SWIG.