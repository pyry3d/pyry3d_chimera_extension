﻿The PyRy3D Software (copyright (c) 2014 by IIMCB and UAM), including associated data and 
documentation (referred to collectively as PyRy3D) has been developed by the contributing 
researchers (referred to collectively as PyRy3D Developers) in the Laboratory of Structural Bioinformatics 
and Protein Engineering headed by Prof. Janusz M. Bujnicki at the International Institute of Molecular and Cell Biology 
in Warsaw (IIMCB) and the Institute of Molecular Biology and Biotechnology, Faculty of Biology, Adam Mickiewicz
University in Poznan (UAM).

SimRNAPyRy3D has been developed through support of a variety of funding sources, including the Polish Ministry of 
Science and Higher Education grant N N301 123138 and UMO-2012/05/N/NZ2/01652 to JMK, and by the European Research Council
(StG grant RNA+P=123D) to JMB. JMB has been supported by the "Ideas for Poland" fellowship from the Foundation for 
Polish Science.
Some calculations have been run in Poznań Supercomputing and Networking Center. 
------------------------------------------------------------------------

LEGAL DISCLAIMER

PyRy3D is released under the GPL license, a copy of which is included in 
the distribution (See LICENSE_GPL.TXT for details). For the files in the 
PDB/ directory, the Biopython License applies as well. 
See PDB/LICENSE_BIOPYTHON.TXT for details).

This software is provided "as-is". There are no expressed or implied 
warranties of any kind, including, but not limited to, the warranties of 
merchantability and fitness for a given application. In no event shall 
the authors be liable for any direct, indirect, incidental, special, 
exemplary or consequential damages (including, but not limited to, loss 
of use, data or profits, or business interruption) however caused and on 
any theory of liability, whether in contract, strict liability or tort 
(including negligence or otherwise) arising in any way out of the use 
of this software, even if advised of the possibility of such damage.

The authors take no responsibility for damage caused by this program 
or its components. 

------------------------------------------------------------------------

CREDITS

Joanna M. Kasprzak    - implementation, architecture, tests
Wojciech Potrzebowski - CCP4_reader implementation
Mateusz Dobrychlop    - PyRy3D Chimera Extension and PyRy3D testing
Janusz Bujnicki       - concept and supervision

-----------------------------------------------------------------------

ACKNOWLEDGEMENTS

Credit goes to our lab colleagues Anna Czerwoniec, Kaja Milanowska, 
Anna Philips, Tomasz Puton, Kristian Rother for their comments and 
constructive criticism during development. 
Special thanks to Dominik Kasprzak and Michał Boniecki for advices in Monte Carlo simulations
implementation and for much help in testing and implementation phase of the project.

-------------------------------------------------------------------------

REFERENCES

Components of PyRy3D are based upon the following pieces of scientific literature:
1. Alber F, Förster F, Korkin D, Topf M, Sali A., , Integrating diverse data for structure determination of macromolecular assemblies., Rev Biochem.
2. Sali A, Glaeser R, Earnest T, Baumeister W., , From words to literature in structural proteomics., Nature



