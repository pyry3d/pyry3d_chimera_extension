#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# 
# www.genesilico.pl 
#

#creates ranked 3D models of macromoleular complexes 
#based on experimental restraints and a whole complex shape.

__author__ = "Joanna M. Kasprzak"
__copyright__ = "Copyright 2010, The PyRy3D Project"
__credits__ = ["Janusz Bujnicki"]
__license__ = "GPL"
__version__ = "0.1.0"
__maintainer__ = "Joanna Kasprzak"
__email__ = "jkasp@amu.edu.pl"
__status__ = "Prototype"


from numpy import array
from random import uniform
from shutil import rmtree
import os
from copy import deepcopy

from Modules.Input.Input            import Input
from Modules.Config.Config          import Config
from Modules.Trans.Trans            import Trans
from Modules.Trans.Component        import Component
from Modules.Simul.Simul            import Simul
from Modules.Simul.Complex          import PyRyComplex
from Modules.Constans.Logfile       import logfile, trafl, movehist, outfolder

import sys, os, traceback
from optparse                                          import OptionParser
from External_Applications.filtrest3D.RestraintsParser import *
from External_Applications.filtrest3D.PModel           import PModel, EmptyStructure
from External_Applications.filtrest3D.ModelSet         import *
from DDownloader                                       import * 
from Modules.Error.Errors                              import InputError

inp   =    Input()  #represents data delivered by the user
trans =    Trans()  #represents user data tranformed into Pyry objects
con   =    Config() #represents user defined simulation parameters

def arrange_components_in_map_center(components, density_map):
    """
        puts all components inside simulbox after rearangment all center of masses
        are localized inside a density map cuboid center(this centralization is
        for statistical purposes only; to calculate number of grid cells
        necesarry to describe each component)
    Parameters:
    ----------
        components  : list of all complex components
        density_map : density map object
    """
    for component in components:
        component.pyrystruct.calculate_centre_of_mass()
        vector = array(density_map.mapcuboid.center) - array(component.pyrystruct.center_of_mass)
        component.translate(vector)
            
def arrange_components_inside_simulbox(components, density_map):
    """
        puts all components inside simulbox after rearangment all center of
        masses are randomly localized inside a density map
    Parameters:
    ----------
        components  : list of all complex components
        density_map : density map object
    """
    for component in components:
        #print "!!@@", component.pyrystruct.chain, component.moves.rot_ranges, component.moves.max_trans_vec, component.moves.limited
        if component.moves.state == "fixed" or component.moves.limited == True: #or movable with restrained moves
            #print "Component moved to center but it was fixed!!", component.pyrystruct.chain, component.trans_history
            vector = component.trans_history
            component.translate(-vector)
            #print "Now - should be [0 0 0]", component.trans_history
        else:
            #print "locate component randomly in map", component.pyrystruct.chain
            component.pyrystruct.calculate_centre_of_mass()
            random_vector = calculate_translation_vector(density_map)
            vector = array(random_vector) - array(component.pyrystruct.center_of_mass)
            component.translate(vector)
            #print "history", component.trans_history
            
def calculate_translation_vector(density_map):
    """
        calculates how to translate particular component in order
        to locate it inside the mapcuboid new position is chosen randomly
    Parameters:
    ----------
        density_map :   density map Complex_map object
    Returns:
    --------
        random_vector   : randomly chosen [x,y,z] coordinates inside mapcuboid
    """
    random_x = uniform(density_map.mapcuboid.xmin, density_map.mapcuboid.xmax)
    random_y = uniform(density_map.mapcuboid.ymin, density_map.mapcuboid.ymax)
    random_z = uniform(density_map.mapcuboid.zmin, density_map.mapcuboid.zmax)
    random_vector = [random_x, random_y, random_z]
    return random_vector
            
def create_first_complex(components, start_anntemp, traflfile):
    """
        Sets the initial structure components start_anntemp :
        starting simulation temperature
    """

#----- call first complex, assign components and start temperature ---------
    
    pc = PyRyComplex (components)
    print "----", len(pc.components)
    pc.temp = con.anntemp
    pc.set_penalties_weights(con.collision_penalty[0],\
                             con.restraints_penalty[0],\
                             con.free_space_penalty[0],\
                             con.out_box_penalty[0])
    
# -------------------- add first complex to outfiles ----------------------    
    logfile.write_message("cx score "+str(round(pc.simulation_score,3))+"\tcomponents "+str(round(pc.restraints,3))+" "+\
                         str(round(pc.clashes,3))+" "+str(round(pc.empty_score,3)))
# -------------------------------------------------------------------------

#---- set complex properties before first simulation run -------
    for component in pc.components:
        #print "MOVES...", component.pyrystruct.chain,component.moves.allowed_transform
        #print "MOVES...cd", component.moves.rot_ranges_def, component.moves.trans_ranges_def
        component.clear_pyryatoms()
        component.simulate_disorder()
        component.set_centre_of_mass()        
        pc.add_simul_change(component)
        pc.add_movable_component(component)
        pc.add_disordered_atoms(component)
        pc.get_alfa_atoms(component)
        pc.add_complex_atoms(len(list(component.pyrystruct.struct.get_atoms())))
        pc.add_complex_alfa_atoms(len(component.alfa_atoms))
    
    pc.calculate_complex_volume ()
    pc.generate_comp_pairs()
    density_map = trans.get_map(inp.saxsfile, inp.mapfile, pc, con)
    pc.save_pdb('original_structure')
    
    original_structure = deepcopy(pc)
    arrange_components_in_map_center(pc.components, density_map)
    pc.get_simboxcells(density_map)
    arrange_components_inside_simulbox(pc.components, density_map)

    pc.calculate_simulation_score(trans.interactions, density_map, con)
    pc.save_pdb('inside_map')
    pc.clean_vollist()
    plugin_scorelist=[str(round(pc.simulation_score,3)),
                      str(round(pc.restraints_penalty*pc.restraints,3)),
                      str(round(pc.collision_penalty*pc.clashes,3)),
                      str(round(pc.freespace_penalty*pc.empty_score,3)),
                      str(round(pc.outbox_penalty*pc.outbox,3))]
# --------------------------------------------------------------------

    #del pc
    return pc, density_map, original_structure, plugin_scorelist
   
def get_input_data():
    """
        calls Input Module methods to get data delivered by the user i.e:
        restraints_file  :   file with distance restraints in Filtrest3D format
        structures_files :   tar archive with all pdb structures;
                             one file can store one chain only
        config_file      :   file with simulation parameters defined by the user
        sequence_file    :   plain fasta file with all sequences
    """
    components_names = [] #stores chains names for valid complex componentsd
    for struct in inp.structures:
        pyrystruct, fasta_seq = inp.check_input(struct)
        #generate subunits objects
        trans.get_components(pyrystruct, fasta_seq, con) 
        components_names.append(pyrystruct.chain)
        
    ##set componenent with no structure as Pyry3D components    
    components_no_struct = inp.get_seq_with_no_struct()
    for seq_no_struct in components_no_struct:
        fasta_seq = seq_no_struct.seq
        seq_name = seq_no_struct.name
        print "SEQ", fasta_seq, seq_name
        trans.get_components_no_struct(seq_no_struct, con)
    #    components_names.append(seq_name)
    #print "###$$%^^^", components_no_struct
    #not used yet, will be implemented in next generation of PyRy3D
    
def initialize():
    """
       checks if there are any old ouputs in out_structures folder; if so, it
       deletes them if a user do not have out_structures folder a program creates it
    """
    
    # ----remove decoys from previous PyRy run ---------
    if os.path.exists(str(inp.outname)) == True:
        rmtree(str(inp.outname))
        os.mkdir(str(inp.outname))
    else: os.mkdir(str(inp.outname))
    
    #------ set log, trafl, outfolder names ----------
    logfile.set_filename(inp.outname+"/pyry.log")
    if inp.traflfile != None:
        trafl.set_filename(inp.outname+"/"+inp.traflfile+".trafl")
        
    if inp.movehistory_file != None:
        movehist.set_filename(inp.outname+"/"+inp.movehistory_file)
        
    outfolder.set_dirname(inp.outname)

def run_simulation(first_complex, simul_params, interactions, density_map, traflfile):
    """
        calls MC simulation for complex building
    Parameters:
    -----------
        simul_params        : simulation parameters (number of steps,
                              number of out complexes etc.)
        complex_components  : all components objects of analyzed complex
        simulbox_diameter   : maximal simulation box diameter
        sum_dist            : sum of all interactions from restraints file;
                              indicates maximal punishment
    Returns:
    ----------
        complexes           : list of PyRyComplexes after simulation
    """
#-----------perform simulation!-------------------------------------------------
    #decide which mutations are available for the system
#TODO: move to config.py
    available_mutations = ["Rotate", "Translate", "Exchange", "SimulateDisorder"]
    
    if len(first_complex.free) < 2:
        available_mutations.remove("Exchange")
        for index in first_complex.movable:
            if "exchange" in first_complex.components[index].moves.allowed_transform:
                first_complex.components[index].moves.allowed_transform.remove("exchange")
    
    if simul_params.freeze_disorder == str(True):
        available_mutations.remove("SimulateDisorder")
        for component in first_complex.components:
            if "SimulateDisorder" in component.moves.allowed_transform:
                component.moves.allowed_transform.remove("SimulateDisorder")
    #######
    
    
    sim = Simul ()
    sim.setSimulationMethod (simul_params.simmethod)   #@todo Load configuration value
    if simul_params.simmethod == "Genetic": #genetic
        sim.setReductionMethod (simul_params.reductmethod)
        sim.setMaximumPoolSize (100)
    elif simul_params.simmethod == "SimulatedAnnealing": #simulated annealing
        sim.setTemperature (simul_params.anntemp)
    sim.setMutations(available_mutations)
    
    sim.setParameterScalingBoundries([simul_params.param_scaling_range1, \
          simul_params.param_scaling_range2, simul_params.param_scaling_range3])
    sim.setParameterScalingValues([ \
    [simul_params.param_scaling_range1_min, simul_params.param_scaling_range1_max],\
    [simul_params.param_scaling_range2_min, simul_params.param_scaling_range2_max], \
    [simul_params.param_scaling_range3_min, simul_params.param_scaling_range3_max] ])
    
    sim.setMutationFrequecies(simul_params.rotation_freq, simul_params.translation_freq, \
                          simul_params.exchange_freq, simul_params.simul_dd_freq)

    sim.setScalingOption (simul_params.param_scaling)
    sim.setStartingComplex (first_complex)
    sim.setIterationCount (simul_params.simul_steps)
    sim.setIndicators(simul_params)
    sim.setResultCount (simul_params.struct_nr)
    sim.setInteractions (interactions)
    sim.setDensityMap (density_map)
    sim.setStepsToSave(simul_params.niter)
    sim.setTraflfile(traflfile)
    best_complex = sim.start()    
    
#---------take simulation output according to user's wishes---------------------
    #complexes = sim.getResult()
    logfile.write_message("Number of rejected complexes is "+str(sim.rejected))
    return best_complex


def save_fullatom_bestmodel(best_complex):
    """
    takes original complex provided by the user and applies all moves performed
    during simulation to it in order to receive best complex (highest scored)
    in full atom representation
    """
    
    print "Saving fullatom complex. It might take a while. Please be patient...."
    print "best complex is ", best_complex.simulation_score
    
    
    trans2 = Trans()
    components_names = []
    for orig_comp in inp.structures:
        pyrystruct, fasta_seq = inp.check_input(orig_comp)
        #generate subunits objects
        trans2.get_components(pyrystruct, fasta_seq, con, "fullatom") 
        
    original_complex = PyRyComplex(trans2.components)
    #original_complex.save_pdb('original_structure2222')
    
    
    index = 0        
    for component in original_complex.components:
        print "MoveSet history for component:!", best_complex.components[index].pyrystruct.chain, \
        "rotations", best_complex.components[index].rot_history, "translations" ,best_complex.components[index].trans_history
        rothist = deepcopy(best_complex.components[index].rot_history)
        for rot in rothist:
            component.rotate(rot[1], rot[0])
            
        component.translate(best_complex.components[index].trans_history)
        index += 1
    original_complex.save_pdb(1, inp.fullatom_file)
    
class Ranking:

    def __init__(self):
        self.simscore = 0.0
        self.clashes = 0.0
        self.restraints = 0.0
        self.empty_score = 0.0
        self.name = ""
        
    def set_attributes(self, simscore, clashes, restraints, empty_score, name):
        self.clashes = clashes
        self.simscore = simscore
        self.restraints = restraints
        self.empty_score = empty_score
        self.name = name
        
def new_eval(comp_path,seqs,rests,conf,map):
    
    inp   =    Input()  #represents data delivered by the user
    trans =    Trans()  #represents user data tranformed into Pyry objects
    con   =    Config() #represents user defined simulation parameters

    logfile.set_filename("testpyry.log")
    
    test_data = comp_path
    sequences = seqs
    restraints = rests
    config = "d:/portal/input/config.txt"
    map = map
    
    assign_sequences(sequences)
    assign_config(config)
    assign_map(map)
        
        #----- OUTPUT
        
        #inp.outname="d:/portal/input/test_outname"
    
    for test_complex in test_data:
#---geting sequences, structures, restraints and density map provided by the user
        print "Complex", test_complex
        comp_names, modelsets = get_input_data(sequences, test_complex, restraints, config, map)
#-----------set outfolder, logfile and trafl file -----------------------------
        
#------------generate interactions between complex components
        trans.get_interactions(inp.restraints, trans.components)
        
#-----------take simulation parameters or decide to use default values---------
        con.parse_config_file(inp.config, trans.components, comp_names)
        
# ---------- create first complex --------------------------

        first_complex, density_map, original_complex, scorelist = create_first_complex(trans.components, con.anntemp)
        
# ---------- save log messages -------------------------------------------     
        #logfile.write_file()
        
        return scorelist


def evaluate_complex(comp_path,seqs,rests,conf,map):

# ---- call main modules objects! -----------------------------------------    
    inp   =    Input()  #represents data delivered by the user
    trans =    Trans()  #represents user data tranformed into Pyry objects
    con   =    Config() #represents user defined simulation parameters

    #import cProfile
    
#--------------------------- upload user files ----------------------------
    i = 0
    while i == 0:
        modelsets=[]
        #----- SEQUENCES
        inp.sequences=Sequences().get_seqs_data(seqs)

    
        #----- STRUCTURES
    
        comp_path = glob.glob(comp_path+"/*.tar")[0]
        comp_path=comp_path.split("\\")
        comp_path="/".join(comp_path)
        #['d:/portal/input\\input.tar']
        #comp_paths

        inp.structures=Structures().get_structures(comp_path)
        
        modelsets.append(PDBDirfile(comp_path,Structures().pdb_files))
        modelset=CompositeModelSet(modelsets)
        #----- RESTRAINTS
        if rests != None:
            inp.restraints=PyryRestraints().get_restraints(rests,modelset)
        else:
            inp.restraints=[]
        #----- MAP
        #if opts.saxs_filename != None:
        #    saxs = ShapeDescriptor()
        #    inp.saxsfile = saxs.get_saxs_shape(opts.saxs_filename)
        
            
        inp.mapfile=ShapeDescriptor().get_density_map(map)
        
        #----- CONFIG
        
        #inp.config=conf
        inp.config="d:/portal/input/config.txt"
        
        #----- OUTPUT
        
        inp.outname="d:/portal/input/test_outname"
        
        
        inp.fullatom_file = ""
        inp.movehist_file = ""
        
        
        
        #inp.get_data()
        
    #-----------set outfolder, logfile and trafl file -----------------------------
        initialize()
        
    #-----------take simulation parameters or decide to use default values---------
    
        con.parse_config_file(inp.config)
        if inp.movehistory_file:
            con.movehistory = inp.movehistory_file
    
        
    #---geting sequences, structures, restraints and density map provided by the user
        get_input_data()
        
        
    #------------generate interactions between complex components
        
        trans.get_interactions(inp.restraints, trans.components)
            
    # ---------- create first complex --------------------------
    
        first_complex, density_map, original_complex, scorelist = create_first_complex(trans.components, con.anntemp, inp.traflfile)
    #------------perform simulation!-----------------------------------------------
        #best_complex = run_simulation(first_complex, con, trans.interactions, density_map, inp.traflfile) #ODKOMENTOWAC
        #cProfile.run('run_simulation(first_complex, con, trans.interactions, density_map)')
        
        #if inp.fullatom_file:
        #    save_fullatom_bestmodel(best_complex)
        
    # ---------- save log messages -------------------------------------------     
        logfile.write_file()
        #if movehist:   movehist.write_file()
        
        return scorelist
    
        i = 1
    
    
def simulate_complex(comp_path,seqs,rests,conf,map):

# ---- call main modules objects! -----------------------------------------    
    #inp   =    Input()  #represents data delivered by the user
    #trans =    Trans()  #represents user data tranformed into Pyry objects
    #con   =    Config() #represents user defined simulation parameters

    #import cProfile
    
#--------------------------- upload user files ----------------------------
    modelsets=[]
    #----- SEQUENCES
    inp.sequences=Sequences().get_seqs_data(seqs)
    
    #----- STRUCTURES
    
    comp_path = glob.glob(comp_path+"/*.tar")[0]
    comp_path=comp_path.split("\\")
    comp_path="/".join(comp_path)
    #['d:/portal/input\\input.tar']
    #comp_paths
    
    inp.structures=Structures().get_structures(comp_path)
    
    modelsets.append(PDBDirfile(comp_path,Structures().pdb_files))
    modelset=CompositeModelSet(modelsets)
    #----- RESTRAINTS
    if rests != None:
        inp.restraints=PyryRestraints().get_restraints(rests,modelset)
    else:
        inp.restraints=[]
    #----- MAP
    #if opts.saxs_filename != None:
    #    saxs = ShapeDescriptor()
    #    inp.saxsfile = saxs.get_saxs_shape(opts.saxs_filename)
        
    inp.mapfile=ShapeDescriptor().get_density_map(map)
    
    #----- CONFIG
    
    #inp.config=conf
    inp.config="d:/portal/input/config.txt"
    
    #----- OUTPUT
    
    inp.outname="d:/portal/input/test_outname"
    
    inp.fullatom_file = ""
    inp.movehist_file = ""
    
    
    
    #inp.get_data()
    
#-----------set outfolder, logfile and trafl file -----------------------------
    initialize()
    
#-----------take simulation parameters or decide to use default values---------

    con.parse_config_file(inp.config)
    if inp.movehistory_file:
        con.movehistory = inp.movehistory_file
    
#---geting sequences, structures, restraints and density map provided by the user
    get_input_data()
    
#------------generate interactions between complex components
    
    trans.get_interactions(inp.restraints, trans.components)
        
# ---------- create first complex --------------------------
    first_complex, density_map, original_complex = create_first_complex(trans.components, con.anntemp, inp.traflfile)
    
#------------perform simulation!-----------------------------------------------
    best_complex = run_simulation(first_complex, con, trans.interactions, density_map, inp.traflfile) #ODKOMENTOWAC
    #cProfile.run('run_simulation(first_complex, con, trans.interactions, density_map)')
    
    if inp.fullatom_file:
        save_fullatom_bestmodel(best_complex)
    
# ---------- save log messages -------------------------------------------     
    logfile.write_file()
    #if movehist:   movehist.write_file()
    
    print "DONE"
    
def make_ranking(comp_path,seqs,rests,conf,map):

# ---- call main modules objects! -----------------------------------------    
    #inp   =    Input()  #represents data delivered by the user
    #trans =    Trans()  #represents user data tranformed into Pyry objects
    #con   =    Config() #represents user defined simulation parameters

    #import cProfile
    
#--------------------------- upload user files ----------------------------
    modelsets=[]

    #----- SEQUENCES
    inp.sequences=Sequences().get_seqs_data(seqs)
    
    #----- STRUCTURES
    
    comp_path = glob.glob(comp_path+"/*.tar")
    print "SUP?", comp_path
    new_path_list=[]
    for i in comp_path:
        a=i.split("\\")
        b="/".join(a)
        new_path_list.append(b)
    print new_path_list
    #comp_path=comp_path.split("\\")
    #comp_path="/".join(comp_path)
    #['d:/portal/input\\input.tar']
    #comp_paths
    


    #----- MAP
    #if opts.saxs_filename != None:
    #    saxs = ShapeDescriptor()
    #    inp.saxsfile = saxs.get_saxs_shape(opts.saxs_filename)
        
    inp.mapfile=ShapeDescriptor().get_density_map(map)
    
    #----- CONFIG
    
    #inp.config=conf
    inp.config="d:/portal/input/config.txt"
    
    #----- OUTPUT
    
    inp.outname="d:/portal/input/test_outname"
    
    inp.fullatom_file = ""
    inp.movehist_file = ""

    for complex in new_path_list:    
        
        inp.structures=Structures().get_structures(complex)
        
        modelsets.append(PDBDirfile(new_path_list,Structures().pdb_files))
        modelset=CompositeModelSet(modelsets)
        
        #inp.get_data()
            #----- RESTRAINTS
        if rests != None:
            inp.restraints=PyryRestraints().get_restraints(rests,modelset)
        else:
            inp.restraints=[]
        
    #-----------set outfolder, logfile and trafl file -----------------------------
        initialize()
        
    #-----------take simulation parameters or decide to use default values---------
    
        con.parse_config_file(inp.config)
        if inp.movehistory_file:
            con.movehistory = inp.movehistory_file
        
    #---geting sequences, structures, restraints and density map provided by the user
        get_input_data()
        
    #------------generate interactions between complex components
        
        trans.get_interactions(inp.restraints, trans.components)
            
    # ---------- create first complex --------------------------
        first_complex, density_map, original_complex, scorelist = create_first_complex(trans.components, con.anntemp, inp.traflfile)
        
        
    # ---------- save log messages -------------------------------------------     
        logfile.write_file()
        
        print "kompleks przemielony"

    print "DONE"
