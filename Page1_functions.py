import sys, os, shutil
import Pmw
import Tkinter
import tkFileDialog
from Dialogs import Display
from chimera import runCommand, openModels
sys.path.append("C:\Python27\Lib\site-packages")
from PyRy3D_input_generator import InStructures

InSctructures=InStructures()

class P1F:
	def __init__(self):
		pass

	def choosemap(self,maplist):
		p=Tkinter.Tk()
		p.withdraw()
		mappath=tkFileDialog.askopenfilename(parent=p,initialdir="/",title="Choose .map file",filetypes=[("EM and SAXS shape descriptors",".map"),("EM and SAXS shape descriptors",".mrc"),("EM and SAXS shape descriptors",".pdb"),("EM and SAXS shape descriptors",".ccp4")])
		print mappath
		if mappath != "" and mappath != ():
			mapname=mappath.split("/")[-1]
			maplist.settext("#0 "+mapname)
		return mappath

	def choosestruct(self):
		p=Tkinter.Tk()
		p.withdraw()
		slist=[]
		structures=tkFileDialog.askopenfilenames(parent=p,initialdir="/",title="Choose structure files",filetypes=[("pdb files",".pdb")])
		
		#Problem with askopenfilenames: on Windows, it returns a string. On Linux, it returns a list.
		
		if type(structures) is list or type(structures) is tuple: #Linux
			for i in structures:
				slist.append(i)
		else: #Windows
			structures=structures.split(" ")
			for i in structures:
				slist.append(i)
				
		#print "SLIST", slist
		return slist

	def opencommand(self,mappath,slist):
		if mappath!="":
			command1="open "+mappath
			runCommand(command1)
		for i in slist:
			command="open "+i
			runCommand(command)

	def write_via_ig(self,iopath):
		self.check_dir(iopath)
		InStructures.extract_structures(iopath)
		InStructures.create_outfolder(iopath)
		InStructures.prepare_instructures()

	def writeAllPDBs(self,outpath,mappath):
		self.check_dir(outpath)
		opened=openModels.list()
		howmany=len(opened)
		if mappath!="":
			if howmany > 1:
					for m in opened:
						id_=m.oslIdent()[1:]
						#print "id_", id_
						if id_ != "0":
							command="write relative 0 "+id_+ " "+outpath+"/"+id_+".pdb"
							runCommand(command)
		else:
			if howmany>0:
				for m in opened:
					id_=m.oslIdent()[1:]
					#print "id_", id_
					#command="write relative 0 "+id_+ " "+outpath+"/"+id_+".pdb" #I'M NOT REALLY SURE
					command="write "+id_+ " "+outpath+"/"+id_+".pdb"
					runCommand(command)

	def check_dir(self,dire):
		if not os.path.exists(dire):
			os.makedirs(dire)
		else:
			shutil.rmtree(dire)#, ignore_errors=True)
			os.makedirs(dire)

	def clearlist(self,smlist):
		smlist.settext("")

	def define_pyry3d_path(self,parent):
		path=tkFileDialog.askdirectory(parent=parent,initialdir="/",title="Point the PyRy3D directory")
		return path
